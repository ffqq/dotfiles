# dotfiles

This just has my configuration files. will probably add more as time goes on

## Preview
|![Neovim](https://media.discordapp.net/attachments/492780868973101085/945832364225986720/unknown.png?width=717&height=423 "Neovim")|![my bash configuration](https://media.discordapp.net/attachments/635830253335478272/946503882605998200/unknown.png?width=750&height=459 "my bash configuration")|
| ------------ | ------------ |
|![aliases file](https://media.discordapp.net/attachments/492780868973101085/945832364934828062/unknown.png?width=717&height=423 "aliases file")|![bashrc file](https://media.discordapp.net/attachments/492780868973101085/945832599065100318/unknown.png?width=717&height=423 "bashrc file")|

---
### BASH
---
My bash configuration is minimalistic, but it offers great benefits like syntax highlighting, tab completion, and more! My .bashrc file is modular and easy to modify, all it really does is source stuff.

### Related
---
[ble.sh (readline replacement for bash)](https://github.com/akinomyoga/ble.sh "ble.sh (readline replacement for bash)")<br />
[vim-plug (minimal vim plugin manager)](https://github.com/junegunn/vim-plug "vim-plug (minimal vim plugin manager)")<br />
[catppuccin (terminal and nvim color palette)](https://github.com/catppuccin/ "catppuccin (terminal and nvim color palette)")<br />
[nerdtree (a cool tree for vim and neovim)](https://github.com/preservim/nerdtree "nerdtree (a cool tree for vim and neovim)")
