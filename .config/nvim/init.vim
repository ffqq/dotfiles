" Based off of Chick2D's config: https://github.com/chick2d/dotfiles
call plug#begin('~/.config/nvim/plugins')

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'aserebryakov/vim-todo-lists'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
Plug 'chriskempson/base16-vim'
Plug 'mhinz/vim-startify'
Plug 'ap/vim-css-color'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'Iron-E/nvim-highlite'
Plug 'itchyny/lightline.vim'
Plug 'shinchu/lightline-gruvbox.vim'
Plug 'akinsho/nvim-bufferline.lua'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'PhilRunninger/nerdtree-buffer-ops'

call plug#end()

lua << EOF
require("bufferline").setup{}
EOF
let g:neovide_cursor_animation_length=0.03
let g:Hexokinase_highlighters = [ ' background ' ]
let g:lightline = {}
let g:lightline.colorscheme = 'catppuccin'
syntax enable				" Enables syntax highlighting
set guifont="FiraCode Nerd Font Regular:h15"
set cmdheight=1				" Gives more space for coc to display messages 
set encoding=utf-8			" Set internal encoding of vim
set scrolloff=7				" Show 7 lines around the cursorline
set relativenumber			" Enable line numbers
set number
set hidden  				" allow buffer switching without saving
set showtabline =2			" Show tabline always
set clipboard+=unnamedplus		" Enable copy to system clipboard
set mouse=a				" Enable using the mouse
set termguicolors			" Enable colors
set cursorline
set noshowmode
colorscheme catppuccin
autocmd VimEnter * NERDTree | wincmd p
