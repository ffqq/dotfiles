# ffqq#1481
# prompt.bash
# Down with zsh!
[[ $PS1 && -f /usr/local/share/bash-completion/bash_completion.sh ]] && 
    source /usr/local/share/bash-completion/bash_completion.sh
source ~/Documents/Git/ble.sh/out/ble.sh
PROMPT_COMMAND=__coolthing
source ~/.bash_git
__coolthing() {
	case $EUID in
		"0") root ;;
		*) user ;;
	esac
}
root() {
	case $? in
		0) PS1="\[\e[33m\]\u@\h \[\e[36m\][\[\e[34m\]\W$(__git_ps1 "  %s")\[\e[36m\]] \[\e[32m\]Λ\[\e[m\] " ;;
		*) PS1="\[\e[33m\]\u@\h \[\e[36m\][\[\e[34m\]\W$(__git_ps1 "  %s")\[\e[36m\]] \[\e[31m\]Λ\[\e[m\] " ;;
	esac	
}
user() {
	case $? in
		0) PS1="\[\e[31m\]\u@\h \[\e[36m\][\[\e[34m\]\W$(__git_ps1 "  %s")\[\e[36m\]] \[\e[32m\]λ\[\e[m\] " ;;
		*) PS1="\[\e[31m\]\u@\h \[\e[36m\][\[\e[34m\]\W$(__git_ps1 "  %s")\[\e[36m\]] \[\e[31m\]λ\[\e[m\] " ;;
	esac	
}

