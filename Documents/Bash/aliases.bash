# ffqq#1481
# aliases.bash
# Down with zsh!
alias sudo=doas
alias ls="ls --color=auto"
alias "gadd"="git add"
alias "gpush"="git push"
alias "gcomm"="git commit -m"
alias inst="doas pkg install"
alias remove="doas pkg remove"
alias update="doas pkg upgrade"
alias autoremove="doas pkg autoremove"

